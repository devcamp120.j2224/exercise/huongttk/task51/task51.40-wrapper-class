package com.devcamp.j02_javabasic.s40;

public class WrapperExample {
    public static void autoBoxing(){
        byte myByte = 1;
        short myShortNumber = 2;
        int myIntNumber = 3;
        long myLongNumber = 4;
        float myFloatNumber = 5.0F;
        double myDoubleNumber = 6.2;
        boolean myBooleanNumber = false;
        char myCharNumber = 'B';

        Byte byeobj = myByte;
        Short shortobj = myShortNumber;
        Integer intobj = myIntNumber;
        Long longobj = myLongNumber;
        Float floatobj = myFloatNumber;
        Double doubleobj = myDoubleNumber;
        Boolean booleanobj = myBooleanNumber;
        Character charobj = myCharNumber;

        System.out.println("In ra gia tri cua object");
        System.out.println("Byte object: " + byeobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + longobj);
        System.out.println("Float object: " + floatobj);
        System.out.println("Double object: " + doubleobj);
        System.out.println("Boolean object: " + booleanobj);
        System.out.println("Character object: " + charobj);
    }
    public static void unBoxing(){
        byte myByte = 11;
        short myShortNumber = 21;
        int myIntNumber = 31;
        long myLongNumber = 41;
        float myFloatNumber = 51.0F;
        double myDoubleNumber = 61.2;
        boolean myBooleanNumber = true;
        char myCharNumber = 'C';

        Byte byeobj = myByte;
        Short shortobj = myShortNumber;
        Integer intobj = myIntNumber;
        Long longobj = myLongNumber;
        Float floatobj = myFloatNumber;
        Double doubleobj = myDoubleNumber;
        Boolean booleanobj = myBooleanNumber;
        Character charobj = myCharNumber;

        byte bytevalue = byeobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        boolean booleanvalue = booleanobj;
        char charvalue = charobj;

        System.out.println("In ra gia tri cua cac Primitive Data Type(kieu du lieu nguyen thuy)");
        System.out.println("Byte value: " + bytevalue);
        System.out.println("Short value: " + shortvalue);
        System.out.println("Integer value: " + intvalue);
        System.out.println("Long value: " + longvalue);
        System.out.println("Float value: " + floatvalue);
        System.out.println("Double value: " + doublevalue);
        System.out.println("Boolean value: " + booleanvalue);
        System.out.println("Character value: " + charvalue);

    }
    
    public static void main(String[] args) {
        WrapperExample.autoBoxing();
        WrapperExample.unBoxing();
    }

}
